'use strict';

/**
 * @ngdoc overview
 * @name pixelApplicationApp
 * @description
 * # pixelApplicationApp
 *
 * Main module of the application.
 */
var app = angular
  .module('pixelApplicationApp', [
    'homePage',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'daterangepicker',
    'nvd3',
    'ngMaterial',
    'leads'
  ]);


app.config(['$stateProvider', '$locationProvider', function ($stateProvider, $locationProvider) {
  $stateProvider
    .state('homepage', {
      url: '/',
      templateUrl: 'views/homepage.html'
    })
    .state('leads', {
      url: '/leads',
      templateUrl: 'views/leads.html'
    });
  $locationProvider.html5Mode(true);
}]);

app.config(['$routeProvider', '$locationProvider','$httpProvider', function($routeProvider, $locationProvider,$httpProvider) {
  $locationProvider.html5Mode(true);
}]);


app.directive('navbar', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/navbar.html'
  }
});


app.directive('horizontalNavbar', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/horizontalNavbar.html'
  }
});

app.directive('firstRowHomePage', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/firstRowHomePage.html'
  }
});

app.directive('secondRowHomePage', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/secondRowHomePage.html'
  }
});

app.directive('thirdRowHomePage', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/thirdRowHomePage.html'
  }
});


app.directive('homePageAreaCharts', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/homePageAreaCharts.html'
  }
});

app.directive('firstRowLeadsPage', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/firstRowLeadsPage.html'
  }
});

app.directive('thirdRowLeadsPage', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/thirdRowLeadsPage.html'
  }
});

app.directive('fourthRowLeadsPage', function () {
  return {
    restrict: 'A',
    templateUrl: 'views/fourthRowLeadsPage.html'
  }
});
