'use strict';

angular.module('homePage', [])
  .controller('HomePageCtrl', ['$scope', function ($scope) {
    $scope.options = {
      chart: {
        type: 'stackedAreaChart',
        height: 250,
        x: function (d) {
          return d[0];
        },
        y: function (d) {
          return d[1];
        },
        useVoronoi: false,
        clipEdge: true,
        duration: 100,
        useInteractiveGuideline: true,
        xAxis: {
          showMaxMin: false,
          tickFormat: function (d) {
            return d3.time.format('%x')(new Date(d))
          }
        },
        yAxis: {
          tickFormat: function (d) {
            return d3.format(',.2f')(d);
          }
        },
        zoom: {
          enabled: true,
          scaleExtent: [1, 10],
          useFixedDomain: false,
          useNiceScale: false,
          horizontalOff: false,
          verticalOff: true,
          unzoomEventType: 'dblclick.zoom'
        },
        color: function () {
          return 'green';
        },
        showYAxis: false,
        showControls: false,
        showXAxis:true,
        interpolate:'non-linear',
        styles:{
          classes: {
            'with-3d-shadow': true
          }
        }
      }
    };

    $scope.data = [
      {
        "key" : "Leads" ,
        "values" : [ [ 1025409600000 , 23.041422681023] , [ 1030766400000 , 24.02286281168] , [ 1033358400000 , 25.093608385173] , [ 1036040400000 , 25.108079299458] , [ 1038632400000 , 26.982389242348] , [ 1041310800000 , 27.828984957662] , [ 1043989200000 , 28.914055036294] , [ 1046408400000 , 29.436150539916] , [ 1049086800000 , 30.558650338602] , [ 1051675200000 , 31.395594061773] ]
      }
    ];


  }]);
