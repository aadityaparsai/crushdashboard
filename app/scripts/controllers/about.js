'use strict';

/**
 * @ngdoc function
 * @name pixelApplicationApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the pixelApplicationApp
 */
angular.module('pixelApplicationApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
